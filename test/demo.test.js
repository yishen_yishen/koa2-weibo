/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-13 20:32:04
 * @LastEditTime: 2021-01-13 20:36:40
 * @Description: 单元测试
 */

function sum(a, b) {
  return a + b
}

test('test demo 1', () => {
  const res = sum(10, 20)
  expect(res).toBe(30)
})
