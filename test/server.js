/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-13 20:41:00
 * @LastEditTime: 2021-01-13 20:42:44
 * @Description: jest server
 */
const request = require('supertest')
const server = require('../src/app').callback()

module.exports = request(server)
