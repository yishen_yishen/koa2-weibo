/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-13 20:43:06
 * @LastEditTime: 2021-01-13 20:49:34
 * @Description: json test
 */
const { TestScheduler } = require('jest')
const server = require('./server')

test('json 接口返回数据格式正确', async () => {
  const res = await server.get('/json')
  expect(res.body).toEqual({
    title: 'koa2 json',
  })
})
