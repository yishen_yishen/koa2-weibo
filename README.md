# Node.js-Koa2框架生态实战－从零模拟新浪微博

## 简介-前言

* 课程地址：<https://coding.imooc.com/class/388.html>
* 项目架构图

![image-20210122171146521](https://image.mdashen.com/pic/image-20210122171146521.png)

* 数据模型设计

  ![image-20210123152259964](https://image.mdashen.com/pic/image-20210123152259964.png)

## 知识点记录

* 设定路由前缀
`router.prefix('/users')`
* 环境变量

> cross-env NODE_ENV=dev
>
> 不同平台 windows mac linux 设置环境变量的方式不同

### koa2

* 参数

![image-20210106212922794](https://image.mdashen.com/pic/image-20210106212922794.png)

![image-20210106212933747](https://image.mdashen.com/pic/image-20210106212933747.png)

### mysql--sequelize

* 连表查询

![image-20210107211750575](https://image.mdashen.com/pic/image-20210107211750575.png)

### ejs 模板引擎

* 变量

~~~ejs
<!-- 直接打印变量 -->
<p><%= name %></p>
<!-- 没有name变量，打印空/不打印 -->
<p><%= locals.name %></p>
~~~

### redis

> redis：内存数据库。
>
> mysql：硬盘数据库

* 安装：`brew install redis`
* ![image-20210112201334510](https://image.mdashen.com/pic/image-20210112201334510.png)
* 启动、运行 `redis-server` ---->   `redis-cli`
* ![image-20210112201508166](https://image.mdashen.com/pic/image-20210112201508166.png)
* ![image-20210112213724183](https://image.mdashen.com/pic/image-20210112213724183.png)

### jest-单元测试

> 定义：单个功能或接口，给定输入，得到输出。看输出是否符合要求
>
> 需手动编写用例代码，然后统一执行
>
> 意义：能一次性执行所有单测，短时间内验证所有功能是否正常 60

* 命令配置：

  > package.json
  >
  > `"test": "cross-env NODE_ENV=test jest --runInBand --forceExit --colors"`
  >
  > --runInBand：顺序执行
  >
  > --forceExit：执行完退出
  >
  > --colors：彩色输出

### esLint规范

* 安装插件 `npm i eslint babel-eslint -D`
* 配置文件：根目录新建文件 `.eslintignore` 和 `.eslintrc.json`
* `.eslintignore` eslint 忽略的文件

~~~javascript
node_modules
test
src/public
~~~

* `.eslintrc.json` eslint 的规则配置文件 更多eslint规则参考：[github](https://github.com/standard/eslint-config-standard/blob/master/eslintrc.json)

~~~json
{
  "parser": "babel-eslint",
  "env": {
    "es6": true,
    "commonjs": true,
    "node": true
  },
  "rules": {
    "indent": ["error", 4],
    "quotes": ["error", "single", { "allowTemplateLiterals": false }],
    "semi": ["error", "never"]
  }
}

~~~

* package.json scripts 中配置命令

  `"lint": "eslint --ext .js ./src",`

### 调试

`"dev": "cross-env NODE_ENV=dev ./node_modules/.bin/nodemon --inspect=9229 bin/www",`

默认端口：9229

chrome 打开：[chrome://inspect](chrome://inspect)

![image-20210121123018754](https://image.mdashen.com/pic/image-20210121123018754.png)

### jwt

> jwt：json web token
>
> 用户认证成功之后，server端返回一个加密的token给客户端
>
> 客户端后续每次请求头带token，以示当前的用户身份

#### koa2 实现jwt

`npm i koa-jwt`

`npm i jsonwebtoken`

~~~javascript
const jwtKoa = require('koa-jwt')
app.use(jwtKoa({ secret: '随便定义一个字符串，越复杂越好' })).unless({ path: [/^\/users\/login/] })
~~~

### Git 规范化

#### commit -m 规范化

* refactor 重构

  > 重构目录，功能

![image-20210106212059797](https://image.mdashen.com/pic/image-20210106212059797.png)

* feat 新增功能

  ![image-20210106221009492](https://image.mdashen.com/pic/image-20210106221009492.png)

### 数据库

#### 关系型数据库--三大范式

* 属性的原子性：每一列都不可再拆解
* 记录的唯一性：有唯一标识(主键)，其他属性都依赖于主键
* 字段的冗余性：不存在数据冗余和传递依赖