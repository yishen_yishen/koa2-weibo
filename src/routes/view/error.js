/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-21 13:31:03
 * @LastEditTime: 2021-01-21 13:40:21
 * @Description: 错误页面
 */

const router = require('koa-router')()

router.get('/error', async (ctx, next) => {
  await ctx.render('error')
})

// 404
router.get('*', async (ctx, next) => {
  await ctx.render('404')
})

module.exports = router
