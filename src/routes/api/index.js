/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-06 20:29:44
 * @LastEditTime: 2021-01-21 19:32:33
 * @Description:
 */
const router = require('koa-router')()
const jwt = require('jsonwebtoken')

router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!',
  })
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  // const session = ctx.session
  // if ((session.viewNum === null)) {
  //   session.viewNum = 0
  // }
  // session.viewNum++

  ctx.body = {
    title: 'koa2 json',
    // viewNum:session.viewNum
  }
})

router.post('/login', async (ctx, next) => {
  const { user, pwd } = ctx.request.body
})

module.exports = router
