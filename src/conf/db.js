/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-07 22:20:21
 * @LastEditTime: 2021-01-12 20:44:59
 * @Description: 数据库配置信息
 */
const { isProd } = require('../utils/env')
let MYSQL_CONF = {
  host: 'localhost',
  user: 'root',
  password: '',
  port: '3306',
  database: 'koa2_weibo',
}

let REDIS_CONF = {
  port: 6379,
  host: '127.0.0.1',
}
if (isProd) {
  MYSQL_CONF = {
    host: 'localhost',
    user: 'root',
    password: '',
    port: '3306',
    database: 'koa2_weibo',
  }
  REDIS_CONF = {
    port: 6379,
    host: '127.0.0.1',
  }
}

module.exports = {
  MYSQL_CONF,
  REDIS_CONF,
}
