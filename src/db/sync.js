/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-08 22:04:23
 * @LastEditTime: 2021-01-08 22:32:44
 * @Description: 同步数据库模型
 */
const seq = require('./seq')

require('./model/index')

// 测试连接
seq
  .authenticate()
  .then(() => {
    console.log('auth ok')
  })
  .catch(() => {
    console.log('auth err')
  })

// 执行同步 force(强制)/alter(非强制)
seq.sync({ force: true }).then(() => {
  console.log('sync ok')
  process.exit()
})
