/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-08 22:18:14
 * @LastEditTime: 2021-01-08 22:33:56
 * @Description:
 */
const seq = require('../seq')
const DataTypes = require('sequelize')
const Blog = seq.define(
  'blog',
  {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '博客标题',
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '内容',
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '作者id，关联到user.id',
    },
  },
  { tableName: 'blog', comment: '博客表' }
)
module.exports = Blog
