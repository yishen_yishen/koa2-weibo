/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-07 22:22:36
 * @LastEditTime: 2021-01-08 22:16:53
 * @Description: user表数据库模型
 */
const seq = require('../seq')
const DataTypes = require('sequelize')
const User = seq.define(
  'user',
  {
    userName: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '用户名',
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '密码',
    },
    nickName: {
      type: DataTypes.STRING,
      comment: '用户昵称',
    },
  },
  { tableName: 'user', comment: '用户表' }
)
module.exports = User
