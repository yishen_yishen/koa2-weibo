/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-08 22:11:56
 * @LastEditTime: 2021-01-08 22:34:57
 * @Description: 数据库模型集合文件
 */
const User = require('./User')
const Blog = require('./Blog')

//ASK hasMany和belongsTo都写，是为了连表查询
User.hasMany(Blog, {
  foreignKey: 'userId',
})
Blog.belongsTo(User, { foreignKey: 'userId' })

module.exports = {
  User,
  Blog,
}
