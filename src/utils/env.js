/**
 * @Author: yishen@mdashen.com
 * @Date: 2021-01-12 20:37:42
 * @LastEditTime: 2021-01-13 22:44:55
 * @Description: 环境变量配置
 */

const ENV = process.env.NODE_ENV

module.exports = {
  isDev: ENV === 'dev',
  notDev: ENV !== 'dev',
  isProd: ENV === 'production',
  notProd: ENV !== 'production',
  isTest: ENV === 'test',
  notTest: ENV !== 'test',
}
